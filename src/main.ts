import * as vscode from 'vscode';
import { TestHub, testExplorerExtensionId } from 'vscode-test-adapter-api';
import { Log, TestAdapterRegistrar } from 'vscode-test-adapter-util';
import { GoTestAdapter, deleteTempDir } from './adapter';
import { ProfileBenchmarkCodeLensProvider } from './codeLens';

interface ProfileCommandArgs {
    document?: vscode.TextDocument
    functionName?: string
}

export async function activate(context: vscode.ExtensionContext) {
    const workspaceFolder = (vscode.workspace.workspaceFolders || [])[0]

    // create a simple logger, configured with `goTestExplorer.logpanel` and `goTestExplorer.logfile`
    const log = new Log('goTestExplorer', workspaceFolder, 'Go Test Explorer log')
    context.subscriptions.push(log)

    const testExplorerExtension = vscode.extensions.getExtension<TestHub>(testExplorerExtensionId)
    if (!testExplorerExtension) {
        if (log.enabled)
            log.info(`Test Explorer not found`)
        return
    }

    const testExplorer = new TestAdapterRegistrar(testExplorerExtension.exports, ws => new GoTestAdapter(ws, log), log)
    context.subscriptions.push(testExplorer)

    const codeLens = new ProfileBenchmarkCodeLensProvider()
    context.subscriptions.push(vscode.languages.registerCodeLensProvider({ language: 'go', scheme: 'file' }, codeLens))

    vscode.commands.registerCommand('goTestExplorer.profile.cpu.cursor', (args) => executeProfile('cpu', args))

    vscode.commands.registerCommand('goTestExplorer.profile.memory.cursor', (args) => executeProfile('memory', args))

    async function executeProfile(type: 'cpu' | 'memory', args: ProfileCommandArgs): Promise<void> {
        if (!args) return

        const { document, functionName } = args
        if (!document || !functionName) return

        const workspace = vscode.workspace.getWorkspaceFolder(document.uri)
        if (!workspace) return

        const adapter = testExplorer.getAdapter(workspace)
        if (!adapter) return

        await adapter.profile(type, document, functionName)
    }
}

export async function deactivate() {
    await deleteTempDir()
}