import { CodeLensProvider, TextDocument, CancellationToken, CodeLens, commands, SymbolInformation, EventEmitter, workspace } from 'vscode';
import { Configuration } from './config';

export class ProfileBenchmarkCodeLensProvider implements CodeLensProvider {
	private disposables: { dispose(): void }[] = [];
    private changedEmitter = new EventEmitter<void>();

	get onDidChangeCodeLenses() { return this.changedEmitter.event }

    constructor() {
        const sub = workspace.onDidChangeConfiguration(e => {
            if (e.affectsConfiguration(Configuration.section))
                this.changedEmitter.fire()
        })

        this.disposables.push(sub, this.changedEmitter)
    }

    dispose(): void { this.disposables.forEach(d => d.dispose()); this.disposables = [] }

    async provideCodeLenses(document: TextDocument, token: CancellationToken): Promise<CodeLens[]> {
        if (!Configuration.get(document.uri).profiler.showCodeLens)
            return []

        const lenses: CodeLens[] = []
        const symbols: SymbolInformation[] | undefined = await commands.executeCommand('vscode.executeDocumentSymbolProvider', document.uri)
        if (!symbols) return []

        for (const symbol of symbols) {
            if (!/^Benchmark\w+/.test(symbol.name))
                continue

            lenses.push(new CodeLens(symbol.location.range, {
                title: 'cpu profile',
                command: 'goTestExplorer.profile.cpu.cursor',
                arguments: [{ document, functionName: symbol.name }]
            }))

            lenses.push(new CodeLens(symbol.location.range, {
                title: 'memory profile',
                command: 'goTestExplorer.profile.memory.cursor',
                arguments: [{ document, functionName: symbol.name }]
            }))
        }

        return lenses
    }
}